package pl.polsl.urbanek.mateusz.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * View class of Fibonacci calculating app based on MVC design pattern
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public class FiboView {

    /**
     * Declaration of BufferedReader object.
     */
    private BufferedReader bufferedReader;

    /**
     * Declaration of StringBuilder object.
     */
    private StringBuilder stringBuilder;

    /**
     * Default constructor of FiboView class.
     */
    public FiboView() {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        stringBuilder = new StringBuilder();
    }

    /**
     * Takes integer value to print it into System.out. This value represents n-th Fibonacci sequence element.
     *
     * @param value Value of n-th Fibonacci sequence element.
     */
    public void printValue(final int value) {
        stringBuilder.append("Value of Fibonacci sequence for your input: ").append(value);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());
    }

    /**
     * Takes String object to print it into System.out.
     *
     * @param message Message printed to the System.out.
     */
    public void println(final String message) {
        stringBuilder.append(message);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());
    }

    /**
     * Method for reading user input using bufferedReader object.
     *
     * @return String containing user input.
     * @throws IOException Throws IOException.
     */
    public String getln() throws IOException {
        return bufferedReader.readLine();
    }

    /**
     * Method for printing elapsed time for both iterative and recursive methods.
     *
     * @param timeRecursive Long primitive containing elapsed time calculated for recursive method.
     * @param timeIterative Long primitive containing elapsed time calculated for iterative method.
     */
    public void printElapsedTimes(final long timeRecursive, final long timeIterative) {
        stringBuilder.append("Elapsed time for Recursive method in [ns]: ").append(timeRecursive);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("Elapsed time for Iterative method in [ns]: ").append(timeIterative);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());

        stringBuilder.append("Elapsed time difference in [ns]: ").append(timeRecursive-timeIterative);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());
    }

    /**
     * Method for printing elapsed time only for recursive method.
     *
     * @param timeRecursive Long primitive containing elapsed time calculated for recursive method.
     */
    public void printElapsedTimeRecursive(final long timeRecursive) {
        stringBuilder.append("Elapsed time for Recursive method in [ns]: ").append(timeRecursive);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());
    }

    /**
     * Method for printing elapsed time only for iterative method.
     *
     * @param timeIterative Long primitive containing elapsed time calculated for iterative method.
     */
    public void printElapsedTimeIterative(final long timeIterative) {
        stringBuilder.append("Elapsed time for Iterative method in [ns]: ").append(timeIterative);
        System.out.println(stringBuilder);
        stringBuilder.delete(0, stringBuilder.length());
    }
}
