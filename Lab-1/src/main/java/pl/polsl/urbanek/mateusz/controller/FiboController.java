package pl.polsl.urbanek.mateusz.controller;

import pl.polsl.urbanek.mateusz.log.QuickLogger;
import pl.polsl.urbanek.mateusz.model.FiboModel;
import pl.polsl.urbanek.mateusz.view.FiboView;

import java.io.IOException;

import static java.lang.Integer.parseInt;
import static java.lang.System.nanoTime;
import static java.lang.System.exit;
/*
 I know statics are not allowed, but IMHO this will be dumb to create instance of this.
 */
import static pl.polsl.urbanek.mateusz.app.Flags.*;

/**
 * Controller class of app calculating Fibonacci sequence based on MVC design pattern.
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public class FiboController {

    /**
     * Declaration of QuickLogger object.
     */
    private QuickLogger ql;

    /**
     * Declaration of FiboModel object.
     */
    private FiboModel fm;

    /**
     * Declaration of FiboView object.
     */
    private FiboView fv;

    /**
     * Primitive array of beginning of timer for calculating subroutine duration time.
     */
    private long[] timerStart;

    /**
     * Primitive array of end of timer for calculating subroutine duration time.
     */
    private long[] timerEnd;

    /**
     *  Default constructor of FiboController class.
     */
    public FiboController() {
        ql = new QuickLogger();
        fm = new FiboModel();
        fv = new FiboView();

        timerStart = new long[2];
        timerEnd = new long[2];
    }

    /**
     * Main method of application.
     *
     * @param args Command line arguments parsed into application.
     */
    public static void main(String[] args) {
        boolean noInteraction = false;
        boolean quickCalc = false;
        boolean recursionOnly = false;
        boolean iterativeOnly = false;

        FiboController fc = new FiboController();

        int value = 0;

        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case NO_INTERACTION:
                    value = new Integer(args[i+1]);
                    noInteraction = true;
                    fc.ql.log(NO_INTERACTION);
                    break;

                case QUICK_CALC:
                    value = new Integer(args[i+1]);
                    quickCalc = true;
                    fc.ql.log(QUICK_CALC);
                    break;

                case RECURSION_ONLY:
                    recursionOnly = true;
                    fc.ql.log(RECURSION_ONLY);
                    break;

                case ITERATIVE_ONLY:
                    iterativeOnly = true;
                    fc.ql.log(ITERATIVE_ONLY);
                    break;
            }
        }

        if (noInteraction) {
            if (iterativeOnly) {
                fc.calculateAndDisplayIterative(value);
            } else if (recursionOnly) {
                fc.calculateAndDisplayRecurisve(value);
            } else {
                fc.calculateAndDisplay(value);
            }
            exit(0);
        } else if (quickCalc) {
            if (iterativeOnly) {
                fc.calculateAndDisplayIterative(value);
            } else if (recursionOnly) {
                fc.calculateAndDisplayRecurisve(value);
            } else {
                fc.calculateAndDisplay(value);
            }
        }

        String userInput = "";

        fc.fv.println("How many iterations do you want?");
        try {
            userInput = fc.fv.getln();
        } catch (IOException e) {
            fc.ql.log(e.getMessage());
        }

        while (!("exit".equals(userInput))) {
            fc.calculateAndDisplay(parseInt(userInput)); // TODO try/catch

            fc.fv.println("How many iterations do you want?");
            try {
                userInput = fc.fv.getln();
            } catch (IOException e) {
                fc.ql.log(e.getMessage());
            }
        }
    }

    /**
     * Method for calculating fibonacci sequence for n iterations with both iterative
     * and recursive methods, and displaying result elapsed calculation time, and
     * calculation time difference.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplay(final int iterations) {
        int val1, val2;

        timerStart[0] = nanoTime();
        val1 = fm.recursive(iterations);
        timerEnd[0] = nanoTime();

        timerStart[1] = nanoTime();
        val2 = fm.iterative(iterations);
        timerEnd[1] = nanoTime();

        fv.printValue((val1+val2)/2); // TODO test if val1 == val2
        fv.printElapsedTimes(timerEnd[0]-timerStart[0], timerEnd[1]-timerStart[1]);
    }

    /**
     * Method for calculating fibonacci sequence for n iterations only with recursive
     * calculation method, and displaying result and elapsed calculation time.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplayRecurisve(final int iterations) {
        int val;
        timerStart[0] = nanoTime();
        val = fm.recursive(iterations);
        timerEnd[0] = nanoTime();

        fv.printValue(val);
        fv.printElapsedTimeRecursive(timerEnd[0]-timerStart[0]);
    }

    /**
     * Method for calculating fibonacci sequence for n iterations only with iterative
     * calculation method, and displaying result and elapsed calculation time.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplayIterative(final int iterations) {
        int val;
        timerStart[0] = nanoTime();
        val = fm.iterative(iterations);
        timerEnd[0] = nanoTime();

        fv.printValue(val);
        fv.printElapsedTimeIterative(timerEnd[0]-timerStart[0]);
    }
}
