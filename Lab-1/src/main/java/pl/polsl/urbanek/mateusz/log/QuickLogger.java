package pl.polsl.urbanek.mateusz.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple class to perform logging operation (log to System.out).
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public class QuickLogger {

    /**
     * Declaration of DateFormat object.
     */
    private DateFormat dateFormat;

    /**
     * Declaration of Date object.
     */
    private Date date;

    /**
     * Declaration of StringBuilder object.
     */
    private StringBuilder stringBuilder;

    /**
     * Default constructor of QuickLogger class. Initializes dateFormat,
     * date, and stringBuilder objects.
     */
    public QuickLogger() {
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        date = new Date();
        stringBuilder = new StringBuilder();
    }

    /**
     * Method for creating log to System.out.
     *
     * @param message Message to be included in log.
     */
    public void log(final String message) {
        stringBuilder.append("[").append(dateFormat.format(date)).append("] ").append(message);
        System.out.println(stringBuilder);
        stringBuilder.delete(0,stringBuilder.length());
    }

}
