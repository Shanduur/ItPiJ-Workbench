package pl.polsl.urbanek.mateusz.model;

/**
 * Model class of app calculating Fibonacci sequence based on MVC design pattern.
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public class FiboModel {

    /**
     * Iterative calculation of Fibonacci sequence.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     * @return Value of n-th element in Fibonacci sequence.
     */
    public int iterative(final int iterations) {
        if (iterations <= 1) return iterations;

        int currentValue = 1;
        int previousValue = 1;

        for (int i = 1; i<iterations; i++) {
            int temp = currentValue;
            currentValue += previousValue;
            previousValue = temp;
        }

        return currentValue;
    }

    /**
     * Recursive calculation of Fibonacci sequence
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     * @return Value of n-th element in Fibonacci sequence.
     */
    public int recursive(final int iterations) {
        if (iterations <= 1) return iterations;
        return recursive(iterations - 1) + recursive(iterations - 2);
    }
}
