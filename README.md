# ItPiJ-Workbench
Introduction to programming in Java @Macrofaculty / Silesian University of Technology, 2020 (sem.6)

# Lecture 1

## Docs
- docs.oracle.com/javase/specs/
- docs.oracle.com/javase/tutorial/
- coreservlets.com
- docs.oracle.com/javase/10/docs/api/overview-summary.html

## Books
- Head First Java
- Thinking in Java, 4th edition

## Characteristic features
- Object-oriented language
- Java Virtual Machine
- Security
- Garbage collector

```java
s + "some string";
```
This code creates temporary object which after assigning it to variable should be gabage collected.

- IDE / editor
    - `*.java` files - source code 
- compilation
    - `*.class` file - byte code
- interpretation
    - JVM

# Project 1
- `bin` folder,
    - `.jar` file,
    - `.bat` run file,
- `src` folder,
- `doc` folder,
