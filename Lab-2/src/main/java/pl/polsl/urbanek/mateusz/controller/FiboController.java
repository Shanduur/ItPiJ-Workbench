package pl.polsl.urbanek.mateusz.controller;

import pl.polsl.urbanek.mateusz.exceptions.NegativeValueException;
import pl.polsl.urbanek.mateusz.log.QuickLogger;
import pl.polsl.urbanek.mateusz.model.FiboModel;
import pl.polsl.urbanek.mateusz.view.FiboView;

import java.io.IOException;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.System.nanoTime;
import static java.lang.System.exit;
/*
 I know statics are not allowed, but IMHO this will be dumb to create instance of this.
 */
import static pl.polsl.urbanek.mateusz.app.Flags.*;

/**
 * Controller class of app calculating Fibonacci sequence based on MVC design pattern.
 *
 * @author Mateusz Urbanek
 * @version 1.1-SNAPSHOT
 */
public class FiboController {

    /**
     * Declaration of QuickLogger object.
     */
    private QuickLogger quickLogger;

    /**
     * Declaration of FiboModel object.
     */
    private FiboModel fiboModel;

    /**
     * Declaration of FiboView object.
     */
    private FiboView fiboView;

    /**
     * Primitive array of beginning of timer for calculating subroutine duration time.
     */
    private long[] timerStart;

    /**
     * Primitive array of end of timer for calculating subroutine duration time.
     */
    private long[] timerEnd;

    /**
     *  Default constructor of FiboController class.
     */
    public FiboController() {
        this.quickLogger = new QuickLogger(2);
        this.fiboModel = new FiboModel();
        this.fiboView = new FiboView();
        this.timerStart = new long[2];
        this.timerEnd = new long[2];
    }

    /**
     * Main method of application.
     *
     * @param args Command line arguments parsed into application.
     */
    public static void main(String[] args) {
        boolean noInteraction = false;
        boolean quickCalc = false;
        boolean recursionOnly = false;
        boolean iterativeOnly = false;
        boolean sequence = false;

        FiboController fiboController = new FiboController();

        int value = 0;

        for (int i = 0; i < args.length; i++) {
            try {
                switch (args[i]) {
                    case NO_INTERACTION:
                        value = new Integer(args[i+1]);
                        noInteraction = true;
                        fiboController.quickLogger.log(NO_INTERACTION);
                        break;

                    case SEQUENCE:
                        value = new Integer(args[i+1]);
                        sequence = true;
                        fiboController.quickLogger.log(SEQUENCE);
                        break;

                    case QUICK_CALC:
                        value = new Integer(args[i+1]);
                        quickCalc = true;
                        fiboController.quickLogger.log(QUICK_CALC);
                        break;

                    case RECURSION_ONLY:
                        recursionOnly = true;
                        fiboController.quickLogger.log(RECURSION_ONLY);
                        break;

                    case ITERATIVE_ONLY:
                        iterativeOnly = true;
                        fiboController.quickLogger.log(ITERATIVE_ONLY);
                        break;
                }
            } catch (NumberFormatException | NullPointerException e) {
                fiboController.quickLogger.logError(e);
            }
        }

        if (noInteraction && !sequence && !quickCalc) {
            if (iterativeOnly) {
                fiboController.calculateAndDisplayIterative(value);
            } else if (recursionOnly) {
                fiboController.calculateAndDisplayRecursive(value);
            } else {
                fiboController.calculateAndDisplay(value);
            }
            exit(0);
        } else if (quickCalc && !sequence && !noInteraction) {
            if (iterativeOnly) {
                fiboController.calculateAndDisplayIterative(value);
            } else if (recursionOnly) {
                fiboController.calculateAndDisplayRecursive(value);
            } else {
                fiboController.calculateAndDisplay(value);
            }
        } else if (sequence && !noInteraction && !quickCalc) {
            if (!iterativeOnly && !recursionOnly) {
                fiboController.calculateAndDisplayList(value);
            } else exit(1);
        }

        String[] userInputArray = new String[0];
        sequence = false;

        fiboController.fiboView.printLine("How many iterations do you want?\n" +
                "To exit type: exit\n" +
                "To show all numbers in sequence type: seq n\n" +
                "Where n is number.");
        try {
            String input = fiboController.fiboView.getLine();
            userInputArray = input.split(" ");
        } catch (IOException e) {
            fiboController.quickLogger.logError(e);
        }

        for (String itemStr: userInputArray) {
            try {
                if (sequence) {
                    fiboController.calculateAndDisplayList(parseInt(itemStr));
                    sequence = false;
                } else fiboController.calculateAndDisplay(parseInt(itemStr));
            } catch (NumberFormatException e) {
                if("seq".equals(itemStr)) {
                    sequence = true;
                } else if ("exit".equals(itemStr)) {
                    exit(0);
                } else fiboController.quickLogger.logError(e);
            }
        }

        while (true) {
            fiboController.fiboView.printLine("How many iterations do you want?\n" +
                    "To exit type: exit\n" +
                    "To show all numbers in sequence type: seq n\n" +
                    "Where n is number.");
            try {
                String input = fiboController.fiboView.getLine();
                userInputArray = input.split(" ");
            } catch (IOException e) {
                fiboController.quickLogger.logError(e);
                continue;
            }

            for (String itemStr: userInputArray) {
                try {
                    if (sequence) {
                        fiboController.calculateAndDisplayList(parseInt(itemStr));
                        sequence = false;
                    } else fiboController.calculateAndDisplay(parseInt(itemStr));
                } catch (NumberFormatException e) {
                    if("seq".equals(itemStr)) {
                        sequence = true;
                    } else if ("exit".equals(itemStr)) {
                        exit(0);
                    } else {
                        fiboController.quickLogger.logError(e);
                    }
                }
            }
        }
    }

    /**
     * Method for calculating fibonacci sequence for n iterations with iterative
     * method, displaying fibonacci sequence consisting of n elements,
     * and displaying elapsed calculation time.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplayList(final long iterations) {
        List<Long> val = null;

        try {
            timerStart[0] = nanoTime();
            val = fiboModel.iterativeList(iterations);
            timerEnd[0] = nanoTime();
        } catch (NegativeValueException e) {
            quickLogger.log(e.getMessage());
        }

        try {
            fiboView.printList(val);
        } catch (NullPointerException e) {
            e.getMessage();
        }
        fiboView.printElapsedTimeIterative(timerEnd[0]-timerStart[0]);
    }

    /**
     * Method for calculating fibonacci sequence for n iterations with both iterative
     * and recursive methods, and displaying result elapsed calculation time, and
     * calculation time difference.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplay(final long iterations) {
        long val1 = 0;
        long val2 = 0;

        try {
            timerStart[0] = nanoTime();
            val1 = fiboModel.recursive(iterations);
            timerEnd[0] = nanoTime();
        } catch (NegativeValueException e) {
            quickLogger.log(e.getMessage());
        }

        try {
            timerStart[1] = nanoTime();
            val2 = fiboModel.iterative(iterations);
            timerEnd[1] = nanoTime();
        } catch (NegativeValueException e) {
            quickLogger.log(e.getMessage());
        }

        fiboView.printValue((val1+val2)/2); // TODO test if val1 == val2
        fiboView.printElapsedTimes(timerEnd[0]-timerStart[0], timerEnd[1]-timerStart[1]);
    }

    /**
     * Method for calculating fibonacci sequence for n iterations only with recursive
     * calculation method, and displaying result and elapsed calculation time.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplayRecursive(final long iterations) {
        long val = 0;
        try {
            timerStart[0] = nanoTime();
            val = fiboModel.recursive(iterations);
            timerEnd[0] = nanoTime();
        } catch (NegativeValueException e) {
            quickLogger.log(e.getMessage());
        }

        fiboView.printValue(val);
        fiboView.printElapsedTimeRecursive(timerEnd[0]-timerStart[0]);
    }

    /**
     * Method for calculating fibonacci sequence for n iterations only with iterative
     * calculation method, and displaying result and elapsed calculation time.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     */
    private void calculateAndDisplayIterative(final long iterations) {
        long val = 0;
        try {
            timerStart[1] = nanoTime();
            val = fiboModel.iterative(iterations);
            timerEnd[1] = nanoTime();
        } catch (NegativeValueException e) {
            quickLogger.log(e.getMessage());
        }

        fiboView.printValue(val);
        fiboView.printElapsedTimeIterative(timerEnd[0]-timerStart[0]);
    }
}
