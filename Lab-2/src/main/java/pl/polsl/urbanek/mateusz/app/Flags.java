package pl.polsl.urbanek.mateusz.app;

/**
 * Flags used in the process of running app.
 *
 * @author Mateusz Urbanek
 * @version 1.1-SNAPSHOT
 */
public final class Flags {

    /**
     * Flag for running app without interaction for single value.
     * After performing one calculation app exits.
     */
    @Deprecated
    public final static String NO_INTERACTION  = "--no-interaction";

    /**
     * Flag for running app with initial value.
     */
    public final static String QUICK_CALC      = "--quick";

    /**
     * Added after QUICK_CALC or NO_INTERACTION, used to perform only recursive calculations.
     */
    public final static String RECURSION_ONLY  = "--recursive";

    /**
     * Added after QUICK_CALC or NO_INTERACTION, used to perform only iterative calculations.
     */
    public final static String ITERATIVE_ONLY  = "--iterative";

    /**
     * Flag for running app with initial value and displaying sequence.
     */
    public final static String SEQUENCE        = "--sequence";
}
