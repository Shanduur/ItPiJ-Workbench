package pl.polsl.urbanek.mateusz.model;

import pl.polsl.urbanek.mateusz.exceptions.NegativeValueException;

import java.util.ArrayList;

/**
 * Model class of app calculating Fibonacci sequence based on MVC design pattern.
 *
 * @author Mateusz Urbanek
 * @version 1.1-SNAPSHOT
 */
public class FiboModel {

    /**
     * Iterative calculation of Fibonacci sequence.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     * @return Value of n-th element in Fibonacci sequence.
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    public long iterative(final long iterations) throws NegativeValueException {
        if (iterations < 0) throw new NegativeValueException();
        else if (iterations == 0 || iterations == 1) return iterations;

        long currentValue = 1;
        long previousValue = 1;

        for (int i = 1; i<iterations; i++) {
            long temp = currentValue;
            currentValue += previousValue;
            previousValue = temp;
        }

        return currentValue;
    }

    /**
     * Recursive calculation of Fibonacci sequence.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     * @return Fibonacci series list containing n elements.
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    public ArrayList<Long> iterativeList(final long iterations) throws NegativeValueException {
        ArrayList<Long> valuesOfFibonacci = new ArrayList<>();

        if (iterations < 0) throw new NegativeValueException();
        else if (iterations == 0 || iterations == 1) {
            valuesOfFibonacci.add(iterations);
            return valuesOfFibonacci;
        }

        long currentValue = 1;
        long previousValue = 1;

        valuesOfFibonacci.add(previousValue);
        valuesOfFibonacci.add(currentValue);

        for (int i = 1; i<iterations; i++) {
            long temp = currentValue;
            currentValue += previousValue;
            previousValue = temp;

            valuesOfFibonacci.add(currentValue);
        }

        return valuesOfFibonacci;
    }

    /**
     * Recursive calculation of Fibonacci sequence.
     *
     * @param iterations Number of iterations to perform for calculating Fibonacci sequence.
     * @return Value of n-th element in Fibonacci sequence.
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    public long recursive(final long iterations) throws NegativeValueException {
        if (iterations < 0) throw new NegativeValueException();
        else if (iterations == 0 || iterations == 1) return iterations;
        return recursive(iterations - 1) + recursive(iterations - 2);
    }
}
