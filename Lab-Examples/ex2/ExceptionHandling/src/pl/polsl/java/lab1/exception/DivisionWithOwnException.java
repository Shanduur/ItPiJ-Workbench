package pl.polsl.java.lab1.exception;

/**
 * Exception class that is generated when attempting to divide by zero.
 *
 * @author Gall Anonim
 * @version 1.0
 */
class DivisionException extends Exception {

    /**
     * Constructor without parameters
     */
    public DivisionException() {
        // here the super(); constructor is called by default
    }

    /**
     * Constructor
     *
     * @param message display message
     */
    public DivisionException(String message) {
	super(message); // call the constructor of superclass
    }
}

/**
 * Application executes an int divide numbers, which are passed as parameters to
 * main method.
 *
 * @author Gall Anonim
 * @version 1.0
 */
public class DivisionWithOwnException {

    /**
     * Main method of application
     *
     * @param args command line parameters
     */
    public static void main(String args[]) {
	/* value of the dividend */
        int dividend = 0;
        /* divisor value */
        int divisor = 0;
        /* true when an error occurred */
        boolean error = false;

	try {
	    // parse numbers to share
	    dividend = Integer.parseInt(args[0]);
	    divisor = Integer.parseInt(args[1]);
	} // passed the bounds of the args array - not enough arguments
	catch (ArrayIndexOutOfBoundsException e) {
	    error = true;
	    System.out.println("No arguments !!!");
	} // error in the conversion - at least one non-integer given
	catch (NumberFormatException e) {
	    error = true;
	    System.out.println("No numbers of type int!!!");
	} finally {
	    System.out.println("I finished downloading the parameters");
	}

	if (!error) // passed two numbers of type int, divide them
	{
	    try {
		divide(dividend, divisor);	// do division
	    } // custom divide by zero error
	    catch (DivisionException e) {
		System.out.println(e.getMessage());
	    }
	}
    }

    /**
     * The method performs division of two numbers given as a parameter.
     *
     * @param dividend value of the dividend that is split
     * @param divisor value by which the dividend is divided
     * @exception DivisionException when attempt to divide by zero
     */
    static void divide(int dividend, int divisor) throws DivisionException {
        // Method declares that DivisionException may be thrown within its body
	if (divisor == 0) { // division by zero
            // generate new exception using the constructor with String argument
	    throw new DivisionException("Don't divide by zero!!!");
	} else {
            // this division will not generate any error
	    float ratio = (float) dividend / (float) divisor;
	    System.out.println(dividend + " : " + divisor + " = " + ratio);
	}
    }
}
