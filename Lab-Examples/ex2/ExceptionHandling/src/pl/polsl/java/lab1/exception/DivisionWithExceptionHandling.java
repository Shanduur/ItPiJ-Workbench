package pl.polsl.java.lab1.exception;

/**
 * Application executes an <i>int</i> divide numbers, which are passed as
 * parameters to main method.
 *
 * @author Gall Anonim
 * @version 1.0
 */
public class DivisionWithExceptionHandling {

    /**
     * Main method of application.
     *
     * @param args
     */
    public static void main(String args[]) {
        /* value of the dividend */
        int dividend = 0;
        /* divisor value */
        int divisor = 0;
        /* ratio value */
        float ratio = 0;
        /* true when an error occurred */
        boolean error = false;

        try {
            // parse numbers to share
            dividend = Integer.parseInt(args[0]);
            divisor = Integer.parseInt(args[1]);
        } // passed the bounds of the args array - not enough arguments
        catch (ArrayIndexOutOfBoundsException e) {
            error = true;
            System.err.println("No arguments !!!");
        } // error in the conversion - at least one non-integer given
        catch (NumberFormatException e) {
            error = true;
            System.err.println("No numbers of type int!!!");
        } finally {
            /* 
            This code will be executed regardless of what happens
            in the try block
             */
            System.out.println("I finished downloading the parameters");
        }

        if (!error) { // passed two numbers of type int, divide them
            try {
                ratio = dividend / divisor;
            } catch (ArithmeticException e) {// divide by zero error
                error = true;
                System.err.println("Don't divide by zero !!!");
            }

            if (!error) { // division performed properly, so display result
                System.out.println(dividend + " : " + divisor + " = " + ratio);
            }
        }
    }
}
