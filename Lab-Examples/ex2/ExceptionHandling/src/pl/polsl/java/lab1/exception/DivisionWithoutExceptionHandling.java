package pl.polsl.java.lab1.exception;

/**
 * Application executes an <i>int</i> divide numbers, which are passed as
 * parameters to main method.
 *
 * @author Gall Anonim
 * @version 1.0
 */
public class DivisionWithoutExceptionHandling {

    /**
     * Main method of application.
     *
     * @param args
     */
    public static void main(String args[]) {
        /* value of the dividend */
        int dividend = 0;
        /* divisor value */
        int divisor = 0;
        /* ratio value */
        float ratio;

        // At least two arguments should be given
        if (args.length < 2) {
            System.out.println("No arguments !!!");
        } else {
            // Convert Strings to numbers (integers)
            if (args[0].matches("-?[0-9]+") && args[1].matches("-?[0-9]+")) {
                dividend = Integer.parseInt(args[0]);
                divisor = Integer.parseInt(args[1]);
                if (divisor != 0) {
                    /* 
                     It is enough to cast only the divisor, the float type
                     will be expanded automatically to the dividend
                     */
                    ratio = (float) dividend / (float) divisor;
                    System.out.println(dividend + " : " + divisor + " = " + ratio);
                } else {
                    System.out.println("Don't divide by zero !!!");
                }    
            } else System.out.println("Not digit");
            
        }
    }
}
