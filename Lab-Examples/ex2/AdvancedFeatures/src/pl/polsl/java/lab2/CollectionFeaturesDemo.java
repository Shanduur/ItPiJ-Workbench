package pl.polsl.java.lab2;

import java.util.*;
// import of static class members!
import static java.util.Collections.*;

/**
 * This demo resents set of useful methods operating on collections.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class CollectionFeaturesDemo {

    static List<Integer> list = new ArrayList<>();
    static Integer[] tab = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public static void main(String args[]) {
        addAll(list, tab);
        System.out.println("init:      " + list);
        shuffle(list); // randomly reorders elements
        System.out.println("shuffled:  " + list);
        sort(list); // sorts the elements
        System.out.println("sorted:    " + list);
        /* 
        Move all elements by 3 elements to the right, and move the last 3
        at the beginning
         */
        rotate(list, 3);
        System.out.println("rotated:   " + list);
        sort(list);
        System.out.println("sorted:    " + list);
        reverse(list); // reverses the list
        System.out.println("reversed:  " + list);
        swap(list, 1, 2); // swaps 1st and 2nd element
        System.out.println("swapped:   " + list);
        replaceAll(list, 0, 7); // replaces value 0 with 7
        System.out.println("replaced:  " + list);
        System.out.println("Maximum:   " + max(list));
        System.out.println("Minimum:   " + min(list));
        System.out.println("element '7' exists " + frequency(list, 7) + " times");
    }
}
