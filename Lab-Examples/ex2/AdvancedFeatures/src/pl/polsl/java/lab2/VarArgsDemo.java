package pl.polsl.java.lab2;

import static java.lang.System.*;

/**
 * VarArgsDemo feature.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class VarArgsDemo {

    public static void main(String[] args) {
        int[] tab = new int[3]; // array cells initialized with 0 values
        tab[0] = 66;
        // static import allows us to omit the System. at the beginning
        out.println(sum(tab));
        out.println(sum(new int[]{3, 7, 2, 5}));
        out.println(sum(1, 7, 5, 23, 4));
        out.println(sum(1));
        out.println(sum());
    }

    // At compile time, the int... is converted into int[]
    static int sum(int... args) {
        int sum = 0;

        /* 
        The varargs is used just like an array - it has the length and can
        be indexed by []
         */
        for (int i = 0; i < args.length; i++) {
            sum += args[i];
        }
        return sum;
    }
}
