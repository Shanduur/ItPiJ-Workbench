package pl.polsl.java.lab2;

/**
 * Demonstration of autoboxing.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class AutoboxingDemo {

    public static void main(String[] args) {

        Integer obj = 17; // box, the assignment
        /* 
        The following two operations are similar 
        to operator overloading in C++
         */
        obj++; // unpacking, increment, packaging
        obj += 3; // unpacking, adding, packaging

        // summing the simple types and objects
        Integer a = new Integer(12);
        int b = 7;
        Integer c1 = a + b;
        int c2 = a + b;
        System.out.println("" + c1 + ", " + c2);

        fun(4);
    }

    public static void fun(Integer arg) {
        System.out.println("Integer");
    }
    
    public static void fun(double arg) {
        System.out.println("double");
    }
    
    public static void fun(int arg) {
        System.out.println("int");
    }
    
    public static void fun(int... args) {
        System.out.println("int...");
    }
}
