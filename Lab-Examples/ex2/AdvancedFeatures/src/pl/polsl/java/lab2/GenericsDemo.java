package pl.polsl.java.lab2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Generics features.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class GenericsDemo {

    public static void main(String[] args) {

        // standard ("raw") collection
        Vector vector = new Vector();
        vector.add(66);
        vector.add(13);
        // The compiler doesn't check the type of argument 
        vector.add("cat");

        int sum = 0;
        /* 
        Since the collection is "raw" we can only use Object type here,
        as each class in Java extends it
         */
//        for (Object element : vector) {
//            // To perform summation a numeric type is needed, hence the cast
//            sum += (Integer) element;
//        }
//        System.out.println("Sum = " + sum);

        // Type-safe (generic) collection  
        Vector<Integer> safeVector = new Vector<>();
        safeVector.add(66);
        safeVector.add(13);
        // The compiler checks the type of argument. Uncomment next line! 
//         safeVector.add("cat");
        /*
         Since the collection is generic we know that only Integers and their 
         subclasses are allowed
         */
        for (int element : safeVector) {
            sum += element;
        }
        System.out.println("Sum = " + sum);

        // own generic type
        Box<String> box1 = new Box<String>();
        Box<Double> box2 = new Box<Double>();
        // Since Java 7 on the right-hand side we can use diamond operator <>
        NumberBox<Number> numberBox = new NumberBox<>();
        numberBox.add(13.0);
        numberBox.add(7.0);
        System.out.println("sum = " + numberBox.sum());

        /* 
        This will generate a compilation error 
        since String does not extend Number
         */
//	NumberBox<String> numberBox3 = new NumberBox<String>();
    }
}

class Box<T> {

    List<T> contents;
}

// N can be any numeric type - any type that is a sublcass of Number class
class NumberBox<N extends Number> {

    List<N> contents;

    NumberBox() {
        contents = new LinkedList<>();
    }

    public void add(N obj) {
        contents.add(obj);
    }

    public N get(int index) {
        return contents.get(index);
    }

    public double sum() {
        double total = 0;
        Iterator<N> i = contents.iterator();
        
        // hasNext() returns false when we reach the end of the collection
        while (i.hasNext()) {
            // next() moves to the next element and returns it
            total += i.next().intValue();
        }
        return total;
    }
}
