package pl.polsl.java.lab2;

/**
 * EnumerationDemo class.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class EnumerationDemo {

    enum Operation {

	PLUS("+"), MINUS("-"), TIMES("*"), DIVIDE("/");
        
        private final String sign;
        
        Operation(String s) {
            sign = s;
        }

        double eval(double x, double y) {
	    switch (this) {
		case PLUS:
		    return x + y;
		case MINUS:
		    return x - y;
		case TIMES:
		    return x * y;
		case DIVIDE:
		    return x / y;
		default:
		    System.err.println("Unknown operation: " + this);
		    return 0;
	    }
	}
        
        @Override
        public String toString() {
            return sign;
        }
    }
    
    
    
    public static void main(String args[]) {
	double x = Double.parseDouble(args[0]);
	double y = Double.parseDouble(args[1]);
	for (Operation op : Operation.values()) {
	    System.out.printf("%.2f %s %.2f = %.2f%n", x, op, y, op.eval(x, y));
	}

    }
}
