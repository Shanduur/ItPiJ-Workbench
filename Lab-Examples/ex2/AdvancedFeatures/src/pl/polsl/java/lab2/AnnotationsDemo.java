package pl.polsl.java.lab2;

import pl.polsl.java.lab2.annotations.Description;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Annotation feature. 
 * 
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class AnnotationsDemo {

    public static void main(String[] args) {
        Sub s = new Sub();
        
        // deprecated method
        s.met2();

        // read your own annotation
        try {
            // Reflection mechanism to load a class with the given name
            Class c = Class.forName("pl.polsl.java.lab2.Sub");
            // Finds the method within the class with the given name
            AnnotatedElement element = c.getMethod("met3");
            // Checks if method is annotated
            if (element.isAnnotationPresent(Description.class)) {
                // Gets the annotation - possible only for RUNTIME policy
                Description desc = element.getAnnotation(Description.class);
                // Reads data given in annotation
                String who = desc.author();
                System.out.println("Author of method is " + who);
                String item = desc.item();
                System.out.println("Item: " + item);
                Description.Importance importance = desc.importance();
                System.out.println("Importance: " + importance);
            } else {
                System.out.println("No annotation!");
            }
        } catch (ClassNotFoundException e) { // thrown if the Class.forName fails
            e.printStackTrace(); // not recommended way of handling the exception
        } catch (NoSuchMethodException e) { // thrown if met3 not found
            e.printStackTrace();
        }
    }
}

// Use of standard annotations
class Base {
    void met() {
    }
}

class Sub extends Base {

    List list;

    Sub() {
        list = new ArrayList();
    }

    @Override // standard annotation used at compile time
    public void met() {
    }

    @Deprecated // can be used on method, field and class level
    void met2() {
    }

    @SuppressWarnings("unchecked") // it is actually @SupressWarnings(value = "...")
    public void collect(Object obj) {
        list.add(obj);
    }

    @Description(item = "Something ...", author = "NOWAK", 
            importance = Description.Importance.HIGH) // use of own annotation
    public void met3() {
    }
}
