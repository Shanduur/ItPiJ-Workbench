package pl.polsl.java.lab2;

import java.util.*;

/**
 * ForEachDemo feature.
 *
 * @author Gall Anonim
 * @version 1.0
 *
 */
public class ForEachDemo {

    public static void main(String[] args) {
        int sum = 0;
        // Array iteration
        int[] tab = {2, 3, 5, 1, 7, 3, 3, 7, 5, 7};
        // For-each loop over an array, translated into for(int i = 0; ...; ...)
        for (int element : tab) {
            sum += element;
        }
        System.out.println(sum);

        // iterating through the collection
        Vector<String> vector = new Vector<>();
        vector.add("dog");
        vector.add("mouse");
        vector.add("cat");
        vector.add("snek");
        vector.add("dog");
        /* 
        For-each loop over a collection, translated into 
        for(Iterator<String> it = vector.iterator(); ...; ...)
         */
        for (String element : vector) {
            System.out.print(element + ", ");
        }
        System.out.println();

        // iterating through the collection
        TreeSet<String> tree = new TreeSet<>();
        tree.add("dog");
        tree.add("mouse");
        tree.add("cat");
        tree.add("snek");
        tree.add("dog");
        for (String element : tree) {
            System.out.print(element + ", ");
        }
        System.out.println();

        // iterating through an object
        Person worker = new Person("Jan", "Nowak", 29, 2222.2f);
        // The object needs to implement the Iterable interface
        for (String atrybut : worker) {
            System.out.print(atrybut + ", ");
        }
        System.out.println();

    }
}

class Person implements Iterable<String>,
        Iterator<String> {

    int age;
    float salary;
    int i, index;
    String name, surname;

    public Person(String name, String surname, int age, float salary) {
        this.age = age;
        this.name = name;
        this.salary = salary;
        this.surname = surname;
    }

    /* 
    The only method from the Iterable interface. 
    Since we implement a generic version, the returned iterator is also generic
    */
    @Override
    public Iterator<String> iterator() {
        index = 0;
        return this;
    }

    // Method of the Iterator interface
    @Override
    public boolean hasNext() {
        return index < 4;
    }

    // Method of the Iterator interface, the return type results from the generic type
    @Override
    public String next() {
        switch (index++) {
            case 0:
                return name;
            case 1:
                return surname;
            case 2:
                return "" + age;
            case 3:
                return "" + salary;
            default:
                return null;
        }
    }

    // Method of the Iterator interface
    @Override
    public void remove() {
    }
}
