package pl.polsl.java.lab1;

/**
 * Interface allows objects to be sorted
 *
 * @author Gall Anonim
 * @version 1.0
 */
interface Sortable {

    /**
     * method for comparing objects
     *
     * @param item compared with the current object
     * @return true current object when it is smaller
     */
    boolean less(Sortable item);

    /*
    Using above method we can compare different objects as long as they 
    implement this interface.
    All interface methods are public abstract by default, and you cannot 
    change it.
    Any fields in an interface are public constants - public static final.
     */
}

/**
 * Class representing a person, and implementing Sortable interface.
 *
 * @author Gall Anonim
 * @version 1.0
 */
class Person implements Sortable {

    /**
     * age
     */
    private final int age;
    /**
     * name
     */
    private final String name;

    /**
     * constructor
     *
     * @param name person name
     * @param age person age
     */
    Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see pl.polsl.java.lab1.Sortable#less(pl.polsl.java.lab1.Sortable)
     */
    @Override // We override the interface method here
    public boolean less(Sortable item) {
        // Perform casting to be able to compare by name and age
        Person person = (Person) item;
        /* 
        Use the Comparable interface method compareTo.
        Recall the strcmp function from C/C++ - they work just the same.
         */
        int value = name.compareTo(person.name);

        if (value < 0) { // name < person.name alphabetically
            return true;
        } else if (value > 0) { // name > person.name
            return false;
        }
        // If names are equal, compare using ages
        return age < person.age;
    }

    /**
     * Converts object's state to the text form
     *
     * @return textual representation of the object
     */
    @Override // We override the method that comes from the java.lang.Object class
    public String toString() {
        return "Name: " + name + " age: " + age;
    }
}

/**
 * Main class of the program.
 *
 * @author Gall Anonim
 * @version 1.0
 */
public class Sorter {

    /**
     * Main method of program
     *
     * @param args command line parameters
     */
    public static void main(String args[]) {
        Person john = new Person("John", 40);
        Person mary = new Person("Mary", 30);

        Sorter sorter = new Sorter();
        Person person = (Person) sorter.min(john, mary);

        //This calls person.toString() implicitly
        System.out.println(person);
    }

    /**
     * Compares two persons and as a result returns the smaller of the two
     *
     * @param one first object in the comparison
     * @param two second object in the comparison
     * @return smaller of two objects given by method parameters
     */
    public Sortable min(Sortable one, Sortable two) {
        /* 
        Standard ternary operator, which checks the condition and returns
        the element after ? if the condition is true, else it returns the element
        after :
         */
        return one.less(two) ? one : two;
    }
}
