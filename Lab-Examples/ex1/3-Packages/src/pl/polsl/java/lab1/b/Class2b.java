package pl.polsl.java.lab1.b;

/**
 * Second class package b.
 * 
 * @author Mateusz Urbanek
 * @version 1.0 
 */
public class Class2b {

    /**
     * The string representing the name of the class.
     */
    private String name;

    /**
     * The initiating constructor of the class name string.
     */
    public Class2b() {
        name = "Class 2b";
    }

    /**
     * @return string containing the name of the class.
     */
    public String getName() {
        return name;
    }
}