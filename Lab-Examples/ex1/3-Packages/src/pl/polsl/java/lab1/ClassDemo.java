package pl.polsl.java.lab1;

// packages containing class definitions 
import pl.polsl.java.lab1.a.*;
import pl.polsl.java.lab1.b.*;
import pl.polsl.java.lab1.c.*;

/**
 * Presenting the use of class belonging to different classes of packets.
 * 
 * @author Mateusz Urbanek
 * @version 1.0 
 */
public class ClassDemo {

    /**
     * Main method of application uses the classes defined in different
     * packages.
     * 
     * @param args command line parameters
     */
    public static void main(String args[]) {
        // calling instances of classes from different packages
        Class1a class1a = new Class1a();
        Class2a class2a = new Class2a();
        Class1b class1b = new Class1b();
        Class2b class2b = new Class2b();
        ClassR c1 = new ClassR("name");
        ClassR c2 = new SubClassR("name");
        ClassR c3 = new SubClassR("names");

        // display the information about the class the different variables
        System.out.println("Class name 1a: " + class1a.getName());
        System.out.println("Class name 2a: " + class2a.getName());
        System.out.println("Class name 1b: " + class1b.getName());
        System.out.println("Class name 2b: " + class2b.getName());
        System.out.println("Class name 2b: " + c1.getName());
        System.out.println("Class name 2b: " + c2.getName());
        System.out.println("Class name 2b: " + c3.getName() + c3.getInstances());

    }
    
    private int i;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
    
    
}