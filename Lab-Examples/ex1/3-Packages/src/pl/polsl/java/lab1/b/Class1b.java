package pl.polsl.java.lab1.b;

/**
 * First class package b.
 * 
 * @author Mateusz Urbanek
 * @version 1.0
 * 
 */
public class Class1b {

    /**
     * The string representing the name of the class.
     */
    private String name;

    /**
     * The initiating constructor of the class name string.
     */
    public Class1b() {
        name = "Class 1b";
    }

    /**
     * @return string containing the name of the class.
     */
    public String getName() {
        return name;
    }
}