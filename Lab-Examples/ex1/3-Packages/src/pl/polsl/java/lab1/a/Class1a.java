// definition of the package in which class is placed
package pl.polsl.java.lab1.a;

/**
 * First class package a.
 * 
 * @author Mateusz Urbanek
 * @version 1.0
 * 
 */
public class Class1a {

    /**
     * The string representing the name of the class.
     */
    private String name;

    /**
     * The initiating constructor of the class name string.
     */
    public Class1a() {
        name = "Class 1a";
    }

    /**
     * @return string containing the name of the class.
     */
    public String getName() {
        return name;
    }
}