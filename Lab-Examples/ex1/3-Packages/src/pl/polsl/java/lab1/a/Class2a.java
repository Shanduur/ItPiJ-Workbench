package pl.polsl.java.lab1.a;

/**
 * Second class packet a.
 * 
 * @author Mateusz Urbanek
 * @version 1.0
 *
 */
public class Class2a {

    /**
     * The string representing the name of the class.
     */
    private String name;

    /**
     * The initiating constructor of the class name string.
     */
    public Class2a() {
        name = "Class 2a";
    }

    /**
     * @return string containing the name of the class.
     */
    public String getName() {
        return name;
    }
}