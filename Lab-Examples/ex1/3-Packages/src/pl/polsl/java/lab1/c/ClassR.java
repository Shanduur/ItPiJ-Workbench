/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.lab1.c;

/**
 *
 * @author Mateusz Urbanek
 */
public class ClassR {
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    private static int instances = 0;

    public static int getInstances() {
        return instances;
    }

    public static void setInstances(int instances) {
        ClassR.instances = instances;
    }
    
    public ClassR(String param) {
        setName(param);
        ClassR.instances++;
    }
    
    public ClassR() {
        ClassR.instances++;
    }

}
