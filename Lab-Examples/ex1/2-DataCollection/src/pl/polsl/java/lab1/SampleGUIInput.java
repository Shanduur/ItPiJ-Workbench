package pl.polsl.java.lab1;

import static javax.swing.JOptionPane.*;

/**
 * Class reading text from the console and prints it on the screen.
 *
 * @author Mateusz Urbanek
 * @version 1.0
 */
public class SampleGUIInput {

    /**
     * Gets a string from standard input and prints the text on standard output.
     * 
     * @param args command line parameters
     */
    public static void main(String[] args) {
      
        String login = showInputDialog(null, "What is your name?", "Question", QUESTION_MESSAGE);
        showMessageDialog(null, "Hello "+login + "!", "Invitation", INFORMATION_MESSAGE);
    }
}
