/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.java.lab1;

import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Mateusz Urbanek
 */
public class SampleConsoleInput {

    /**
     * Gets a string from standard input and prints the text on standard output.
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter double: ");
        //String login = scanner.next();
        //System.out.println("Hello " + login);
        
        
        int size = 2;
        Double[] array = new Double[size];
        
        for (int i = 0; i < array.length; i++) {
            array[i] = new Double(scanner.next());
        }
        
        for (Double doubly : array) {
            System.out.println(doubly);
        }
        
        
        double[] d = new double[size];
        for (int i = 0; i < d.length; i++) {
            d[i] = scanner.nextDouble();
        }
        
//        for (double doubly : d) {
//            System.out.println(doubly);
//        }
        
        System.out.println(Arrays.toString(d));

    }
}
