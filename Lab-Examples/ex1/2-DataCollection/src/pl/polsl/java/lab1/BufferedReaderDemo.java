package pl.polsl.java.lab1;

import java.io.*;

/**
 * Class reading from the console text and prints it on screen.
 *
 * @author Mateusz Urbanek
 */
public class BufferedReaderDemo {

    /**
     * Gets a string from standard input using special stream objects, 
     * next prints the text on standard output.
     *
     * @param args program parameters
     * @throws IOException when IO operation fails. The exception should be
     * handled in a real application.
     */
    public static void main(String[] args) throws IOException {
        System.out.print("What is your name? ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String name = in.readLine();
        System.out.println("Hello " + name);
    }
}
