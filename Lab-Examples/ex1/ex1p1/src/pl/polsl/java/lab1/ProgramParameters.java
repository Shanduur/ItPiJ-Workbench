package pl.polsl.java.lab1;

import java.io.IOException;

/**
 * Class call up writing out the parameters for the console output.
 * 
 * @author Mateusz Urbanek
 * @version 1.0
 */
public class ProgramParameters {
    /**
     * Main method of writing out the parameters.
     * 
     * @param args program call parameters
     */
    public static void main(final String args[]) {
        // number of parameters passed to the program
        final int paramtersNumber = args.length;

        System.out.println("Program parameters: ");

        // write all the parameters passed to the program
        for (int i = 0; i < paramtersNumber; i++) {
            System.out.println("parameter " + (i + 1) + ":" + args[i]);
        }
                
        System.out.println(ProgramParameters.instances);
        
        ProgramParameters p = new ProgramParameters();
        
        System.out.println(ProgramParameters.instances);
        
        try {
            int b = System.in.read();
            String x = String.valueOf(b);
            p.printString(x);
        } catch(IOException e) {
            System.out.println(e.getCause());
        }
        
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < args.length; i++) {
            sb.append("Parameter ").append(i).append(": ").append(args[i]);
            System.out.println(sb);
            sb.delete(0,sb.length());
        }
        
    }
    
    
    /**
     * This is constructor
     */
    public ProgramParameters() {
        ProgramParameters.instances++;
    }
    
    /**
     * This string contains name
     */
    private final String STRING_CONTAINING_NAME = "Name"; 
    
    /**
     * This int contains number of instances
     */
    public static int instances = 0;

    /**
     * Method printing string to System out and returning Boolean after 
     * comparison with default string.
     * 
     * @param str String taken by function.
     * @return Boolean value true if equals str
     * @throws IOException something something
     */
    public boolean printString(final String str) throws IOException {
        System.out.println(str);
        return "str".equals(str);
    }
    
    /**
     * Getter method
     * 
     * @return int returns something
     */
    public int getSomething() {
        return something;
    }
    
    /**
     * Setter method
     * 
     * @param param takes value to be et
     * @return returns true on success
     */
    public boolean setSomething(final int param) {
        this.something = param;
        
        return this.something == param;
    }
    
    /**
     * Something int
     */
    private int something;
}
