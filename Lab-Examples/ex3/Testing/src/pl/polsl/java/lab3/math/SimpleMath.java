package pl.polsl.java.lab3.math;

/**
 * Simple class performing multiplication
 * 
 * @author Gall Anonim
 * @version 1.0
 */
public class SimpleMath {

    /**
     * Multiplies two integers
     * 
     * @param a first operand
     * @param b second operand
     * @return result of multiplication
     */
    public int multiply(int a, int b) {
        return a * b;
    }
}
