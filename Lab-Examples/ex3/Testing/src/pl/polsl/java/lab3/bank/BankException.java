package pl.polsl.java.lab3.bank;

/**
 * Exception class representing all abnormal situations in bank operations
 * 
 * @author Gall Anonim
 * @version 1.0
 */
public class BankException extends Exception {
}
