package pl.polsl.java.lab3.categories;

import pl.polsl.java.lab3.categories.markers.ExtraTests;
import pl.polsl.java.lab3.categories.markers.SlowTests;
import org.junit.*;
import org.junit.experimental.categories.*;

@Category({SlowTests.class, ExtraTests.class})
public class TestCaseB {

    @Test
    public void testB() {
        // fail(" test B ");
    }
}
