package pl.polsl.java.lab3.categories;

import org.junit.experimental.categories.*;
import org.junit.experimental.categories.Categories.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import pl.polsl.java.lab3.categories.markers.ExtraTests;
import pl.polsl.java.lab3.categories.markers.SlowTests;


@RunWith(Categories.class)
@IncludeCategory(SlowTests.class)
@ExcludeCategory(ExtraTests.class)
@SuiteClasses({ TestCaseA.class, TestCaseB.class })
// Note that Categories is a kind of Suite
public class SlowNotExtraTestSuite {
  // Will run TestCaseA.testA2  
  // but not TestCaseA.testA1 and not TestCaseB.testB,
}