package pl.polsl.java.lab3.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    AccountTest.class,
    TestAssert.class,
    TestSequence.class,})
public class TestSuite {
}
