package pl.polsl.java.lab3.parameters;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import pl.polsl.java.lab3.math.SimpleMath;

@RunWith(Parameterized.class)
public class ParameterizedTest {

    @Parameter(0)
    public int parameterA;
    
    @Parameter(1)
    public int parameterB;

//    public ParameterizedTest(int parameterA, int parameterB) {
//        this.parameterA = parameterA;
//        this.parameterB = parameterB;
//    }

    @Parameters(name = "{index}: {0} & {1}")
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
            {0, 0}, {1, 0}, {121, 0}
        };
        return Arrays.asList(data);
    }

    @Test
    public void testMultiply() {
        SimpleMath simpleMath = new SimpleMath();
        //System.out.println("A: " + parameterA + "   B: " + parameterB);
        assertEquals("Result", 0,
                simpleMath.multiply(parameterA,
                        parameterB));
    }
}
