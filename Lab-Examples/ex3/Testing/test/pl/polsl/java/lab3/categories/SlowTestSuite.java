package pl.polsl.java.lab3.categories;

import pl.polsl.java.lab3.categories.markers.SlowTests;
import org.junit.experimental.categories.*;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import pl.polsl.java.lab3.categories.markers.ExtraTests;


@RunWith(Categories.class)
@IncludeCategory(SlowTests.class)
@ExcludeCategory(ExtraTests.class)
@SuiteClasses({ TestCaseA.class, TestCaseB.class })
// Note that Categories is a kind of Suite
public class SlowTestSuite {
  // Will run TestCaseA.testA2 and TestCaseB.testB, 
  // but not TestCaseA.testA1
}
