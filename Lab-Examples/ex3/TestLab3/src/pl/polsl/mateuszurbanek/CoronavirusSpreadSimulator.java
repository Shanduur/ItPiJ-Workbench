package pl.polsl.mateuszurbanek;

public class CoronavirusSpreadSimulator {

    private static final int INFECTION_RATIO = 3;
    
    private final int noOfInfected;
    private final int noOfMeetings;
    
    public CoronavirusSpreadSimulator(int noOfInfected, int noOfMeetings) {
        this.noOfInfected = noOfInfected;
        this.noOfMeetings = noOfMeetings;
    }    
    
    public long simulateSpread() throws InvalidDataException {
        long result = noOfInfected;
        
        if (noOfMeetings < 0) 
            throw new InvalidDataException("Non positive number of meetings."); 
        
        if (noOfInfected < 0)
            throw new InvalidDataException("Non positive number of infected.");
        
        for (int i = 0; i < noOfMeetings; i++) {
            result *= INFECTION_RATIO;
        }

        return result;
    }
    
}

class InvalidDataException extends Exception {
    
    public InvalidDataException(String message) {
        super(message);
    }
}