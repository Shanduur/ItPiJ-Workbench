package pl.polsl.mateuszurbanek;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoronavirusSpreadSimulatorTest {
    
    CoronavirusSpreadSimulator simulator;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSimulateSpread() throws InvalidDataException {
        simulator = new CoronavirusSpreadSimulator(1, 1);
        
        assertEquals(simulator.simulateSpread(), 3);
    }
    
    @Test(expected = InvalidDataException.class)
    public void testSimulateSpreadExceptionalCase1() throws InvalidDataException {
        simulator = new CoronavirusSpreadSimulator(10, -32);
        
        simulator.simulateSpread();
        fail("An exception should occurred during the simulation.");
    }
    
    @Test(expected = InvalidDataException.class)
    public void testSimulateSpreadExceptionalCase2() throws InvalidDataException {
        simulator = new CoronavirusSpreadSimulator(-32, 10);
        
        simulator.simulateSpread();
        fail("An exception should occurred during the simulation.");
    }
    
    @Test
    public void testSimulateSpreadBorderCase1() throws InvalidDataException {
        simulator = new CoronavirusSpreadSimulator(0, 10);
        
        assertEquals(simulator.simulateSpread(), 0);
    }
    
    @Test
    public void testSimulateSpreadBorderCase2() throws InvalidDataException {
        simulator = new CoronavirusSpreadSimulator(10, 0);
        
        assertEquals(simulator.simulateSpread(), 10);
    }
}
