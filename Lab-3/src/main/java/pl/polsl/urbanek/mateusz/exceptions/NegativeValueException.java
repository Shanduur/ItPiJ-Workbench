package pl.polsl.urbanek.mateusz.exceptions;

/**
 * Throwable class extending Exception, which is being thrown by method which should not
 * take negative arguments.
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public final class NegativeValueException extends Exception {

    /**
     * Default constructor of NegativeValueException class.
     */
    public NegativeValueException() {
        super("Negative provided number while calculating Fibonacci sequence.");
        // It calls super constructor by default
    }

    /**
     * Constructor that allows specifying custom message held by class.
     *
     * @param message Specifies another message that informs about the type
     * of exception.
     */
    public NegativeValueException(String message) {
        super(message);
    }
}
