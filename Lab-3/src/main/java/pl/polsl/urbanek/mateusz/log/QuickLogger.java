package pl.polsl.urbanek.mateusz.log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Simple class to perform logging operation (log to System.out).
 *
 * @author Mateusz Urbanek
 * @version 1.1-SNAPSHOT
 */
public class QuickLogger {

    /**
     * Declaration of DateFormat object.
     */
    private DateFormat dateFormat;

    /**
     * Declaration of Date object.
     */
    private Date date;

    /**
     * Defines level of log.
     * If value is smaller 1 then log is paused.
     * If value is equal 1 then only message is logged.
     * If value is bigger 1 then logging time and date in addition to message.
     */
    private int logLevel;

    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }

    /**
     * Default constructor of QuickLogger class. Initializes dateFormat,
     * date, and stringBuilder objects.
     *
     * @param logLevel Defines level of verbosity.
     */
    public QuickLogger(int logLevel) {
        this.dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        this.date = new Date();
        this.logLevel = logLevel;
    }

    /**
     * Method used for creating log to System.out.
     *
     * @param message Message to be included in log.
     */
    public void log(String message) {
        System.out.print(createStringLog(message));
    }

    /**
     * Method used for creating log of exception to System.out.
     *
     * @param exception Exception for which log is created.
     */
    public void logError(Exception exception) {
        System.out.print(createStringLog(exception.toString()));
        if (logLevel > 1) {
            exception.printStackTrace();
        }
    }

    /**
     * Method used for combining message with date and time according to log level.
     *
     * @param message Carries message from which creates log.
     * @return returns composed message according to log level.
     */
    private String createStringLog(String message) {
        if (logLevel < 1) return "[" + dateFormat.format(date) + "] " + message + '\n';
        else if (logLevel == 1) return message + '\n';
        else return "";
    }

}
