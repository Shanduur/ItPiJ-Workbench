package pl.polsl.urbanek.mateusz.model;

import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.Collection;
import java.util.zip.DataFormatException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import pl.polsl.urbanek.mateusz.exceptions.NegativeValueException;
import pl.polsl.urbanek.mateusz.model.FiboModel;

/**
 * Class of parametrized test.
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
@RunWith(Parameterized.class)
public class FiboModelParametrizedTest {

    /**
     * First parameter for testing.
     */
    @Parameter(0)
    public int inputParameter;

    /**
     * Second parameter for testing.
     */
    @Parameter(1)
    public int resultParameter;


    /**
     * Declaration of FiboModel object.
     */
    FiboModel fiboModel;

    /**
     * Method for assigning values to parameters.
     */
    @Parameters(name = "{index}: {0} & {1}")
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {0, 0}, {1, 1}, {2, 1}, {10, 55}, {11, 89}
        };
        return Arrays.asList(data);
    }

    /**
     * Testing Model for multiple input and output combinations.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Test
    public void testBoth() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(resultParameter, fiboModel.recursive(inputParameter));
        assertEquals(resultParameter, fiboModel.iterative(inputParameter));
    }
}
