package pl.polsl.urbanek.mateusz.tester;

import org.junit.experimental.categories.Categories.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import pl.polsl.urbanek.mateusz.model.FiboModelTest;
import pl.polsl.urbanek.mateusz.model.FiboModelParametrizedTest;

@RunWith(Suite.class)
@IncludeCategory(BorderCaseTest.class)
@SuiteClasses({
        FiboModelTest.class,
        FiboModelParametrizedTest.class
})
public class TestSuite {
}
