package pl.polsl.urbanek.mateusz.model;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import pl.polsl.urbanek.mateusz.exceptions.NegativeValueException;
import pl.polsl.urbanek.mateusz.tester.BorderCaseTest;
import pl.polsl.urbanek.mateusz.tester.ExceptionalTest;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Test case of FiboModel class
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public class FiboModelTest {

    /**
     * Declaration of FiboModel object.
     */
    FiboModel fiboModel;

    /**
     * Testing normal behavior of iterative implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Test
    public void testIterative() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(55, fiboModel.iterative(10));
    }

    /**
     * Testing normal behavior of recursive implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Test
    public void testRecursive() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(55, fiboModel.recursive(10));
    }

    /**
     * Testing normal behavior of both iterative and recursive implementations.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Test
    public void testBoth() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(fiboModel.recursive(10), fiboModel.iterative(10));
    }

    /**
     * Testing normal behavior of iterative list implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Test
    public void testIterativeList() throws NegativeValueException {
        fiboModel = new FiboModel();
        ArrayList<Long> arrayList = new ArrayList<>();
        ArrayList<Long> result;

        arrayList.add(1L);
        arrayList.add(1L);
        arrayList.add(2L);
        arrayList.add(3L);
        arrayList.add(5L);
        arrayList.add(8L);
        arrayList.add(13L);
        arrayList.add(21L);
        result = fiboModel.iterativeList(10);

        for (int i = 0; i < arrayList.size(); i++) {
            assertEquals(arrayList.get(i), result.get(i));
        }
    }

    /**
     * Testing exceptional case for recursive implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(ExceptionalTest.class)
    @Test(expected = NegativeValueException.class)
    public void testExceptionalRecursive() throws NegativeValueException {
        fiboModel = new FiboModel();

        fiboModel.recursive(-10);
        fail("Exception should be thrown.");
    }

    /**
     * Testing exceptional case for recursive implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(ExceptionalTest.class)
    @Test(expected = NegativeValueException.class)
    public void testExceptionalIterative() throws NegativeValueException {
        fiboModel = new FiboModel();

        fiboModel.iterative(-10);
        fail("Exception should be thrown.");
    }

    /**
     * Testing exceptional case for iterative list implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(ExceptionalTest.class)
    @Test(expected = NegativeValueException.class)
    public void testExceptionalIterativeList() throws NegativeValueException {
        fiboModel = new FiboModel();

        fiboModel.iterativeList(-10);
        fail("Exception should be thrown.");
    }

    /**
     * Testing border cases for iterative implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(BorderCaseTest.class)
    @Test
    public void testBorderCaseIterative() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(0, fiboModel.iterative(0));
        assertEquals(1, fiboModel.iterative(1));
    }

    /**
     * Testing border cases for recursive implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(BorderCaseTest.class)
    @Test
    public void testBorderCaseRecursive() throws NegativeValueException {
        fiboModel = new FiboModel();

        assertEquals(0, fiboModel.recursive(0));
        assertEquals(1, fiboModel.recursive(1));
    }

    /**
     * Testing border cases for iterative list implementation.
     *
     * @throws NegativeValueException Throws this exception if number of iterations is negative,
     * which is not allowed.
     */
    @Category(BorderCaseTest.class)
    @Test
    public void testBorderCaseIterativeList() throws NegativeValueException {
        fiboModel = new FiboModel();

        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add((0L));

        ArrayList<Long> result = fiboModel.iterativeList(0);

        for (int i = 0; i < arrayList.size(); i++) {
            assertEquals(arrayList.get(i), result.get(i));
        }

        result = fiboModel.iterativeList(1);
        arrayList.clear();
        arrayList.add(1L);

        for (int i = 0; i < arrayList.size(); i++) {
            assertEquals(arrayList.get(i), result.get(i));
        }
    }
}
