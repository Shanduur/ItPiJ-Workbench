package pl.polsl.urbanek.mateusz.tester;

/**
 * Marker interface for Exceptional tests
 *
 * @author Mateusz Urbanek
 * @version 1.0-SNAPSHOT
 */
public interface ExceptionalTest {
}
